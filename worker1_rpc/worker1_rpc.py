from nameko.rpc import rpc, RpcProxy
from time import sleep
from nameko.events import EventDispatcher
from nameko.standalone.rpc import ClusterRpcProxy
import json

config = {
    'AMQP_URI': "amqp://guest:guest@rabbit:5672"
}

class ClusterService:
    name = "worker1_rpc"
    dispatch = EventDispatcher()
    @rpc
    def hello_sleep(self, message):
        print("Hello Sleep called")
        sleep(1)
        #json_data = {'name': 'Arindam1', 'id':' 0xffsss'}
        json_data = {'name': message}
        self.dispatch("say_hello_sleep", json.dumps(json_data))
        #with ClusterRpcProxy(config=config) as worker1_rpc:
        #    try:
        #        worker1_rpc.roy_rpc.say_hello.call_async("Cluster_RPC")
        #        #worker1_rpc.roy_rpc.say_hello.call_async("Cluster_RPC")
        #    except Exception as e:
        #        print("Exception while trying to call rpc")
        #        pass
        return json_data

    @rpc
    def hello_no_sleep(self, message):
        print("Hello No Sleep called")
        return "Hello, {}!".format(message)
        #json_data = {'name': 'Arindam', 'id':' 0xffsss'}
        #self.dispatch("say_hello", json.dumps(json_data))
