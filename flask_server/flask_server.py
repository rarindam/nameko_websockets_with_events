from flask import Flask
from nameko.standalone.rpc import ClusterRpcProxy
from flask import jsonify
app = Flask(__name__)

config = {
    'AMQP_URI': "amqp://guest:guest@rabbit:5672"
}

@app.route('/hello_no_sleep')
def hello_no_sleep():
	data = "Hello World"
	with ClusterRpcProxy(config) as cluster_rpc:
		data = cluster_rpc.cluster_service1.hello_no_sleep("Called from hello_no_sleep")
	return data

@app.route('/hello_sleep')
def hello_sleep():
	data = "Hello Sleep"
	with ClusterRpcProxy(config) as cluster_rpc:
		data = cluster_rpc.worker1_rpc.hello_sleep("Called from hello_sleep")
	return jsonify(data)
if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8000', debug=True)
