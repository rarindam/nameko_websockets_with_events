from nameko.events import event_handler
import json
import time
"""
from kombu import Exchange, Queue
from nameko.messaging import Publisher, consume
MY_ROUTING_KEY = "my_routing_key"
my_exchange = Exchange(name="my_exchange")

my_queue = Queue(exchange=my_exchange, routing_key=MY_ROUTING_KEY, name=MY_ROUTING_KEY)
"""

from nameko.events import EventDispatcher

class event_listener:
    name = 'event_listener'
    """
    my_publisher = Publisher(queue=my_queue)
    """
    dispatch = EventDispatcher()

    @event_handler("worker1_rpc", "say_hello_sleep")
    def handle_event(self, payload):
        data = json.dumps(payload)
        print(data)
        """
        self.my_publisher(payload, routing_key=MY_ROUTING_KEY)
        """
        self.dispatch("evt.recvd.hello_sleep", data)
