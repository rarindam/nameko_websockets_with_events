#!/bin/bash

docker build --network=host -t flask_server flask_server
docker build --network=host -t worker1_rpc worker1_rpc
docker build --network=host -t event_listener event_listener
docker build --network=host -t websocket_server websocket_server
docker build --network=host -t websocket_server websocket_server
docker-compose up -d
