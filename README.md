The aim of the project is to find out how to use Nameko for our requirements.
The requirements are simplified to the following points

1. Have a Flask based rest api server, which will be catering to all the
requests coming from frontend

2. Have a websocket server (implemented using Nameko in this case), which
will update the browser, notifiying of any events.

3. Currently, this is how the POC works:
   a. From one browser window, user needs to open "http://<ip>:8000/"
      1. This connects to a websocket server
      2. This page will be updated by the websocket server
   
   b. From another browser window, open "http://<ip>:80/hello_sleep"
      1. This connects with the rest server.
      2. The rest server internally calls an rpc asynchronously.
      3. The rpc (served by worker1_rpc), sleeps for 1 second, and
         dispatches an event ("say_hello_sleep").
      4. This event is captured by event_listener.
      5. Event listener in turn dispatches "evt.recvd.hello_sleep"
      6. This event is received by Wesbsocket server.
      7. The Websocket server updates the connected browser, with
         any data that was received from the Event.
         
4. The POC demonstrates the following:
    a. How to use RPC asynchronously and implement them using Nameko
    b. How to listen and dispatch events using Nameko
    c. How combine websockets and Event listener using Nameko.
    d. Also provides a demo JS code to use the websocket server.

5. Issues observed:
    a. Sometimes the connections to the rabbitmq from other entities,
    are flaky. Hence doing docker-compose up -d twice, helps in having
    all the containers running properly.
    
    b. Need to handle connection refused errors from Nameko.